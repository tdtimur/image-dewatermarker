import cv2
import keras_ocr
import math
import numpy as np
import re

from cog import BasePredictor, Input, Path
from PIL import Image
from tempfile import NamedTemporaryFile
from typing import Tuple


class Predictor(BasePredictor):
    def setup(self) -> None:
        self.pipeline = keras_ocr.pipeline.Pipeline()

    @classmethod
    def midpoint(cls, x1: int, y1: int, x2: int, y2: int) -> Tuple[int, int]:
        x_mid = int((x1 + x2) / 2)
        y_mid = int((y1 + y2) / 2)
        return x_mid, y_mid

    def remove_watermark(self, image: np.ndarray, regex: str) -> np.ndarray:
        prediction_groups = self.pipeline.recognize([image])
        mask = np.zeros(image.shape[:2], dtype="uint8")

        for box in prediction_groups[0]:
            label = box[0].lower()
            matched = re.search(regex, label)
            if matched is None:
                continue
            x0, y0 = box[1][0]
            x1, y1 = box[1][1]
            x2, y2 = box[1][2]
            x3, y3 = box[1][3]

            x_mid0, y_mid0 = self.midpoint(x1, y1, x2, y2)
            x_mid1, y_mi1 = self.midpoint(x0, y0, x3, y3)

            thickness = int(math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))

            cv2.line(mask, (x_mid0, y_mid0), (x_mid1, y_mi1), 255, thickness)
            image = cv2.inpaint(image, mask, 7, cv2.INPAINT_NS)
        return image

    def read_watermarks(self, image: np.ndarray) -> str:
        prediction_groups = self.pipeline.recognize([image])
        mask = np.zeros(image.shape[:2], dtype="uint8")
        res = []

        for box in prediction_groups[0]:
            res.append(box[0])
        return ','.join(res)

    def predict(
            self,
            image: Path = Input(description="The input image"),
            regex: str = Input(description="Regex rule to be used in `re.search(regex, detected_label)`"),
    ) -> Path:
        img = Image.open(image).convert("RGB")
        img_array = np.asarray(img)
        result = self.remove_watermark(img_array, regex)
        with NamedTemporaryFile('w+b', delete=False, suffix='.jpeg') as f:
            Image.fromarray(result).save(f.name)
            return Path(f.name)
        # return self.read_watermarks(img_array)

